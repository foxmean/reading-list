# Reading List (not included SE, Seminar of Lacan, Écrits)
- [] Frosh, S, (2010) Psychoanalysis Outside the Clinic. London: Palgrave.
- [] Gay, P. (1988) Freud: A Life for our Time. London: Dent.
- [] Makari, G. (2008) Revolution in Mind. New York: Harper.
- [] Roudinesco, E. (2016) Freud in His Time and Ours. Cambridge, Mass.: Harvard University Press.
- [] Zaretski, E. (2004) Secrets of the Soul: A Social and Cultural History of Psychoanalysis. London: Vintage.
- [] Fink, B. (1999) A Clinical Introduction to Lacanian Psychoanalysis. New York: Harvard University Press.
- [] Grosz, E. (1990) Jacques Lacan: A Feminist Introduction. London: Routledge.
- [] Hook, D. ((2018) Six Moments in Lacan. London: Routledge.
- [] Parker, I. (2011) Lacanian Psychoanalysis: Revolutions in Subjectivity. London: Routledge.
- [] Abel, E. (1989) Virginia Woolf and the Fictions of Psychoanalysis. Chicago: University of Chicago Press
- [] Altman, N., Benjamin, J., Jacobs, T. & Wachtel, P. (2006). Is Politics the Last Taboo in Psychoanalysis? In: L. Layton, N. Hollander and S. Gutwill (Eds.), Psychoanalysis, Class and Politics. London: Routledge.
- [] Anderson, W., Jenson, D. and Keller, R. (eds) (2011) Unconscious Dominions: Psychoanalysis, Colonial Trauma and Global Sovereignties. Durham: Duke University Press.
- [] Benjamin, J. (2017) Beyond Doer and Done To. London: Routledge.
- [] Butler, J. (1997) The Psychic life of Power. Stanford: Stanford University Press.
- [] Butler, J. (2005) Giving an Account of Oneself. New York: Fordham University Press. (Chapters 2 and 3)
- [] Butler, J. (2012) Parting Ways. New York: Columbia University Press. (Chapter 1)
- [] Derrida, J. (1986) Fors: The Anglish Words of Nicolas Abraham and Maria Torok. Foreword to Abraham, N. and Torok, M. (1976) The Wolf Man’s Magic Word: A Cryptonomy. Minneapolis: University of Minnesota Press.
- [] Derrida, J. (1996) Archive Fever. Chicago: University of Chicago Press.
- [] Elliot, A., (1999) Social Theory & Psychoanalysis in Transition: Self and Society from Freud to Kristeva (2nd Edition). London and New York: Free Association Books.
- [] Fanon, F. (1952) Black Skin, White Masks. London: Pluto.
- [] Fassin, D. and Rechtman, R. (2009) The Empire of Trauma. New Jersey: Princton University Press. (Chapter 3)
- [] Felman, S. (ed) (1982) Literature and Psychoanalysis: The Question of Reading: Otherwise. Baltimore: Johns Hopkins University Press.
- [] Frosh, S. (1999) The Politics of Psychoanalysis. London: Macmillan.
- [] Frosh, S. (2010) Psychoanalysis Outside the Clinic. London: Palgrave.
- [] Frosh, S. (2012) A Brief Introduction to Psychoanalytic Theory. London: Palgrave.
- [] Frosh, S. (2013) Hauntings: Psychoanalysis and Ghostly Transmissions. London: Palgrave.
- [] Frosh, S. (2019) Those Who Come After: Postmemory, Acknowledgement and Forgiveness. London: Palgrave.
- [] Gabbard, G. (2001) (ed) Psychoanalysis and Film. London: Karnac
- [] Gentile, K., (2016) The Business of Being Made: The temporalities of reproductive technologies, in psychoanalysis and culture. London & New York: Routledge.
- [] Greedharry, A. (2008) Postcolonial Theory and Psychoanalysis. London: Palgrave.
- [] Hook, D. (2012) A Critical Psychology of the Postcolonial: The Mind of Apartheid. London: Routledge.
- [] Hook, D. (2013) (Post)Apartheid Conditions: Psychoanalysis and Social Formation. Houndmills: Palgrave
- [] Kaplan, E. (ed) (1990) Psychoanalysis and Cinema. London: Routledge
- [] Khanna, R. (2003) Dark continents: Psychoanalysis and colonialism. Durham: Duke University Press. (Chapters 2 & 4)
- [] Mulvey, L. (1975) Visual Pleasure and Narrative Cinema. In L. Mulvey, Visual and Other Pleasures. London: Macmillan, 1989
- [] Palacios, M. (2013) Radical Sociality. London: Palgrave. (Chapter 6)
- [] Rose, J. (2003) Apathy and Accountability: The Challenge of South Africa’s Truth and Reconciliation Commission to the Intellectual in the Modern World. In J. Rose, On Not Being Able to Sleep. London: Verso.
- [] Rose, J. (2007) The Last Resistance. London: Verso, Chapters 1 and 3.
- [] Royal, N. (2003) The Uncanny. Manchester: Manchester University Press.
- [] Rustin, M.J. (1991) The Good Society and the Inner World. London: Verso. (Chapter 3)
- [] Rustin, M.J. (2002) Give me a Consulting Room: the generation of psychoanalytic knowledge. In M.J. Rustin, Reason and Unreason: Psychoanalysis, Science and Politics. London: Continuum Books.
- [] Rustin, M.J. (2010) Looking for the Unexpected: Psychoanalytic Understanding and Politics. British Journal of Psychotherapy 26, 472-479.
- [] Ryan, J., (2017) Psychoanalysis and Class: Landscapes of Inequality. London and New York: Routledge.
- [] Segal, H. (1995) From Hiroshima to the Gulf War and After: A Psychoanalytic Perspective. In A. Elliott and S. Frosh (eds) Psychoanalysis in Contexts. London: Routledge
- [] Salamon, G., (2010) Assuming a Body: Transgender and Rhetorics of Materiality. New York: Columbia University Press
- [] Seshadri-Crooks, K. (2000) Desiring Whiteness: A Lacanian Analysis of Race. New York: Routledge.
- [] Trezise, T. (2013) Witnessing Witnessing. New York: Fordham University Press. (Chapter 2).
- [] Wynter, Sylvia (2001) ‘Towards the Sociogenic Principle: Fanon, the Puzzle of Conscious Experience, and What It Is Like to Be ‘Black’’, pp. 30–66 in Mercedes F. Duran-Cogan and Antonio Gómez-Moriana (eds)., National Identities and Socio -political Changes in Latin America. New York: Routledge
- [] S. Freud, ‘An Autobiographical Sketch’ in P. Gay, The Freud Reader (New York, 1989)
- [] S. Frosh, Key Concepts in Psychoanalysis (London, 2002)
- [] E. Hobsbawm, The Age of Empire: 1875-1914 (London, 1987)
- [] E. Hobsbawm, The Age of Extremes: The Short Twentieth Century, 1914-1991 (London, 1994)
- [] J. Lear, Freud (London, 2005)
- [] M. Mazower, Dark Continent: Europe’s Twentieth Century (London, 1999)
- [] Fink, Bruce. 2017. A Clinical Introduction to Freud: Techniques for Everyday Practice (New York, NY: WW Norton)- []